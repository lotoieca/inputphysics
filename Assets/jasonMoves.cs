using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jasonMoves : MonoBehaviour
{
    public float movingForce;
    private AudioSource choqueSound;
    // Start is called before the first frame update
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector2 dir = new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y) * movingForce;
        gameObject.GetComponent<Rigidbody2D>().AddForce(dir);
        choqueSound = GameObject.Find("Choque").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            choqueSound.Play();
            collision.gameObject.GetComponent<respawnPlayer>().resetPosition();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            Destroy(gameObject);
        }
    }
}
