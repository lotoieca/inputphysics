using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForceEvent : MonoBehaviour
{
    public float jumpForce;

    private Rigidbody2D rig;
    private void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rig.AddForce(new Vector2(0, jumpForce), ForceMode2D.Force);
        }
    }
}
