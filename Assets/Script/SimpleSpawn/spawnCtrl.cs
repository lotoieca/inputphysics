using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnCtrl : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject jason;
    public float timeToSpanw;

    private bool shoot = false;
    private float time;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (shoot && time+timeToSpanw < Time.realtimeSinceStartup)
        {
            Instantiate(jason, spawnPoint.position,Quaternion.identity);
            spawnPoint.gameObject.GetComponent<AudioSource>().Play();
            time = Time.realtimeSinceStartup;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            shoot = true;
            time = Time.realtimeSinceStartup - timeToSpanw;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            shoot = false;
        }
    }
}
