using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    public float shootForce = 400;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Vector2 dir = new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y) * shootForce;
        gameObject.GetComponent<Rigidbody2D>().AddForce(dir);

        Destroy(gameObject, 3f);
    }
}
