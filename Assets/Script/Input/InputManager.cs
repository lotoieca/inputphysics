using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Get Horizontal movement
        float moveHorizontal = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

        //Get Horizontal movement
        float moveVertical = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        transform.Translate(new Vector3(moveHorizontal,moveVertical,0));
    }
}
