using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleInputKey : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("A Key GetKey");
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("A Key GetKeyDown");
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            Debug.Log("A Key GetKeyUp");
        }

        //0 Derecho
        //1 Izquierdo
        //2 Medio
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Click Derecho");
        }

        //Horizontal
        //Vertical
        if (Input.GetAxis("Horizontal") != 0)
        {
            Debug.Log("HorizontalAxis activated");
        }

        //Virtual Buttons
        //space
        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("Jump Activated");
        }
    }
}
