using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCtrl : MonoBehaviour
{
    public float movementSpeed;
    

    private Rigidbody2D rig;
    
    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float moveDir = Input.GetAxis("Horizontal") * movementSpeed;
        rig.AddForce(new Vector2(moveDir,0));
    }
}
