using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpCtrl : MonoBehaviour
{
    public float jumpForce;
    private Rigidbody2D rig;
    private bool isLandend = true;
    private AudioSource jumpSound;
    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
        jumpSound = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && isLandend)
        //if (Input.GetButtonDown("Jump"))
        {
            rig.AddForce(new Vector2(0, jumpForce));
            jumpSound.Play();
            isLandend = false;
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor" && !isLandend)
        {
            isLandend = true;
        }
    }
}
