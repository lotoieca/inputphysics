using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respawnPlayer : MonoBehaviour
{
    private Rigidbody2D rig;
    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Respawn")
        {
            Debug.Log("trigger activado");
            resetPosition();
        }
    }

    public void resetPosition()
    {
        transform.position = new Vector3(0, 0, 0);
        rig.velocity = Vector3.zero;
        rig.angularVelocity = 0f;
    }
}
