using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sfxCtrl : MonoBehaviour
{
    private AudioSource click;
    // Start is called before the first frame update
    void Start()
    {
        click = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetButtonDown("Jump"))
        {
            click.Play();
        }       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        click.Play();
    }
}
